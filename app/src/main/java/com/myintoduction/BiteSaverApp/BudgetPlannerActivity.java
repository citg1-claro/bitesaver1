package com.myintoduction.BiteSaverApp;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.TextView;

import com.myintoduction.budgetbuddy.R;

import java.util.Locale;

public class BudgetPlannerActivity extends AppCompatActivity {

    private EditText incomeInput;
    private TextView budgetLimitText;
    private Button saveButton;
    private float incomeAmount;
    private float expensesAmount;
    private float budgetLimit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.budget_planner_activity);

        incomeInput = findViewById(R.id.income_input);
        budgetLimitText = findViewById(R.id.budget_limit_text);
        saveButton = findViewById(R.id.save_button);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Calculate budget limit
                incomeAmount = Float.parseFloat(incomeInput.getText().toString());
                budgetLimit = incomeAmount - expensesAmount;

                // Update budget limit text
                budgetLimitText.setText(String.format(Locale.getDefault(), "Budget Limit: $%.2f", budgetLimit));
            }
        });
    }
}