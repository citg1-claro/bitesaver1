package com.myintoduction.BiteSaverApp;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.myintoduction.budgetbuddy.R;
public class GoalTrackingActivity extends AppCompatActivity {
    private EditText goalNameInput, goalAmountInput;
    private Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goal_tracking);

        // Get references to views
        goalNameInput = findViewById(R.id.goal_name_input);
        goalAmountInput = findViewById(R.id.goal_amount_input);
        saveButton = findViewById(R.id.save_button);

        // Set click listener for save button
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get values from input fields
                String goalName = goalNameInput.getText().toString();
                String goalAmountString = goalAmountInput.getText().toString();

                // Validate input
                if (goalName.isEmpty() || goalAmountString.isEmpty()) {
                    Toast.makeText(GoalTrackingActivity.this, "Please fill in all fields", Toast.LENGTH_SHORT).show();
                    return;
                }

                double goalAmount = Double.parseDouble(goalAmountString);

                // Save goal to database or shared preferences
                // ...

                // Show success message
                Toast.makeText(GoalTrackingActivity.this, "Goal saved", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
