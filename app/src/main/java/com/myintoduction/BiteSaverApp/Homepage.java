package com.myintoduction.BiteSaverApp;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.myintoduction.budgetbuddy.R;

public class Homepage extends AppCompatActivity {

    private Button expenseTrackerButton;
    private Button budgetPlannerButton;
    private Button goalTrackingButton;
    private Button transactionHistoryButton;
    private Button savingsCalculatorButton;

    private TextView name_text_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_page);

        String name = getIntent().getStringExtra("name");


        name_text_view = findViewById(R.id.name_text_view);

        String helloText = "Hello " + name;
        name_text_view.setText(helloText);

        expenseTrackerButton = findViewById(R.id.expense_tracker_button);
        budgetPlannerButton = findViewById(R.id.budget_planner_button);
        goalTrackingButton = findViewById(R.id.goal_tracking_button);
        transactionHistoryButton = findViewById(R.id.transaction_history_button);
        savingsCalculatorButton = findViewById(R.id.savings_calculator_button);




        expenseTrackerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Homepage.this, ExpenseTrackerActivity.class);
                startActivity(intent);
            }
        });

        budgetPlannerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Homepage.this, BudgetPlannerActivity.class);
                startActivity(intent);
            }
        });

        goalTrackingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Homepage.this, GoalTrackingActivity.class);
                startActivity(intent);
            }
        });

        transactionHistoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Homepage.this, TransactionHistoryActivity.class);
                startActivity(intent);
            }
        });

        savingsCalculatorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Homepage.this, SavingsCalculatorActivity.class);
                startActivity(intent);
            }
        });
    }
}
