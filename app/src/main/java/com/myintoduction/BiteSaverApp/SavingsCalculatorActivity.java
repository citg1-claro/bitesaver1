package com.myintoduction.BiteSaverApp;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import com.myintoduction.budgetbuddy.R;
import java.text.NumberFormat;
import java.util.Locale;

public class SavingsCalculatorActivity extends AppCompatActivity {

    EditText incomeInput, expensesInput;
    TextView savingsText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.savings_calculator);

        incomeInput = findViewById(R.id.income_input);
        expensesInput = findViewById(R.id.expenses_input);
        savingsText = findViewById(R.id.savings_text);

        Button calculateButton = findViewById(R.id.calculate_button);
        calculateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculateSavings();
            }
        });
    }

    private void calculateSavings() {
        double income = Double.parseDouble(incomeInput.getText().toString());
        double expenses = Double.parseDouble(expensesInput.getText().toString());
        double savings = income - expenses;

        NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.getDefault());
        String savingsFormatted = formatter.format(savings);

        savingsText.setText("Your monthly savings: " + savingsFormatted);
    }
}
