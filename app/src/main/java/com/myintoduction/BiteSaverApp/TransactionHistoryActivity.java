package com.myintoduction.BiteSaverApp;



import androidx.appcompat.app.AppCompatActivity;
import com.myintoduction.budgetbuddy.R;
import android.os.Bundle;
import android.widget.TextView;

public class TransactionHistoryActivity extends AppCompatActivity {

    private TextView mTransactionHistoryTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history);

        mTransactionHistoryTextView = findViewById(R.id.transaction_history_text_view);

        // retrieve transaction history data from a database or file and display it in the TextView
        // for demonstration purposes, we will just set some dummy text
        mTransactionHistoryTextView.setText("Transaction 1\nTransaction 2\nTransaction 3");
    }
}
