package com.myintoduction.BiteSaverApp;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.myintoduction.budgetbuddy.R;
public class ExpenseTrackerActivity extends AppCompatActivity {

    // Declare UI elements
    private EditText expenseInput;
    private Spinner categorySpinner;
    private Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expense_tracker);

        // Get references to UI elements
        expenseInput = findViewById(R.id.expense_input);
        categorySpinner = findViewById(R.id.category_spinner);
        saveButton = findViewById(R.id.save_button);

        // Set up click listener for save button
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get the entered expense amount and category
                String expense = expenseInput.getText().toString();
                String category = categorySpinner.getSelectedItem().toString();

                // Save the expense in a database or file
                saveExpense(expense, category);

                // Clear the input fields
                expenseInput.setText("");
                categorySpinner.setSelection(0);
            }
        });
    }

    private void saveExpense(String expense, String category) {
        // This is where you would save the expense in a database or file
        // For simplicity, we'll just display a toast message with the expense and category
        Toast.makeText(this, "Expense saved: " + expense + " - " + category, Toast.LENGTH_SHORT).show();
    }
}

